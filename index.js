const express = require('express')
const {loggerMiddleware} = require('./utils/logger');
const app = express()
const port = 8080

const {fileRouter} = require('./file/file.router')

app.use(express.json())
app.use(loggerMiddleware)
app.use('/api/files', fileRouter)

app.use((req,res) => {
  res.status(404).json({message:'404 Not found'})
})

app.listen(port, () => console.log(`Server is hosted at http://localhost:${port}`))
