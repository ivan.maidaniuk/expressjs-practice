const fs = require('fs').promises

async function exists (path) {
  try {
    await fs.access(path)
    return true
  } catch {
    return false
  }
}

function getEmptyRequiredFieldName(object, requiredFields) {
  return requiredFields.find(requiredField => {
    return object[requiredField] === undefined
  })
}

module.exports = {
  exists,
  getEmptyRequiredFieldName
}
