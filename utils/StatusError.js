class StatusError extends Error {
  constructor(statusCode, message) {
    super();
    this._statusCode = statusCode
    this._message = message
  }

  get statusCode() {
    return this._statusCode
  }

  get message() {
    return this._message
  }
}

module.exports = {
  StatusError
}
