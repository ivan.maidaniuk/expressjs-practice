const fs = require('fs')

function loggerMiddleware(req, res, next) {
  onResFinishHandle(req, res)
  next()
}

function onResFinishHandle(req, res) {
  const dateStart = new Date()
  res.on('finish', function () {
    const endDate = new Date()
    const executionDuration = endDate.getTime() - dateStart.getTime()

    const dateLogFormat = `${endDate.getDate()}-${endDate.getMonth() + 1}-${endDate.getFullYear()} ` +
        `${endDate.getHours()}:${endDate.getMinutes()}:${endDate.getSeconds()}`

    const logMessage = `${dateLogFormat} -- ${req.method} ${req.originalUrl} ${res.statusCode} - ${executionDuration} ms`
    log(logMessage)
  })
}

function log(message) {
  console.log(message)
  const writeStream = fs.createWriteStream('logFile.log', {flags: 'a'})
  writeStream.write(message + '\n')
  writeStream.end()
}

module.exports = {
  loggerMiddleware,
  log,
}
