const path = require('path')

const {StatusError} = require('../../utils/StatusError');
const {log} = require('../../utils/logger');

class BaseJsonResponse {
  constructor(req, res) {
    this.req = req
    this.res = res
    this.query = {...this.req.query}
    this.params = {...this.req.params}
    this.props = {...this.req.body}
    this.responseData = {}
  }

  async handle() {
    try {
      await this.handleMainCode()
    } catch (error) {
      this.handleError(error)
    } finally {
      this.sendResponse()
    }
  }

  async handleMainCode() {
    // abstract function
  }

  handleError(error) {
    log(error)
    if (error.code === 'ENOENT') {
      return this.handleWrongFileToOpen(error)
    } else if (error.code === 'EISDIR') {
      return this.handleWrongFileNameForCreation(error)
    }
    if (error instanceof StatusError) {
      return this.handleStatusError(error)
    }
    this.responseData.message = 'Server error'
    this.res.status(500)
  }

  handleWrongFileToOpen(error) {
    const pathToFile = path.basename(error.path)
    this.responseData.message = `No file with '${pathToFile}' filename found`
    this.res.status(400)
  }

  handleWrongFileNameForCreation() {
    this.responseData.message = 'Please select acceptable \'filename\' parameter'
    this.res.status(400)
  }

  handleStatusError(error) {
    this.responseData.message = error.message
    this.res.status(error.statusCode)
  }

  sendResponse() {
    this.res.json(this.responseData)
  }
}

module.exports = BaseJsonResponse
