const fs = require('fs').promises;
const path = require('path');

const BaseJsonResponse = require('./BaseJsonResponse');
const {pathToStoragePermissionsFile} = require('../constants');
const {StatusError} = require('../../utils/StatusError');
const {exists} = require('../../utils/helperFunctions');
const {pathToStorage} = require('../constants');

class FileCreation extends BaseJsonResponse {
  async handleMainCode() {
    const pathToFile = path.resolve(pathToStorage, this.props.filename)
    if (!await exists(pathToFile)) {
      if (this.props?.password) {
        await this.createPassword()
      }
      await fs.writeFile(pathToFile, this.props.content)
      this.responseData.message = 'File was created successfully'
    } else {
      throw new StatusError(400, `File with name '${this.props.filename}' is already exit`)
    }
  }

  async createPassword() {
    const permissions = JSON.parse(await fs.readFile(pathToStoragePermissionsFile, 'utf8'))
    permissions[this.props.filename] = this.props.password
    await fs.writeFile(pathToStoragePermissionsFile, JSON.stringify(permissions))
  }
}

const createFile = (req, res) => {
  return new FileCreation(req, res).handle()
}

module.exports = createFile
