module.exports = {
  createFile: require('./createFile'),
  editFileByName: require('./editFileByName'),
  getFiles: require('./getFiles'),
  getFileByName: require('./getFileByName'),
  deleteFile: require('./deleteFile'),
  createStorageFolderIfNotExists: require('./createStorageFolderIfNotExists'),
}
