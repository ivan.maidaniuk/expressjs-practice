const fs = require('fs').promises
const path = require('path')
const BaseJsonResponse = require('./BaseJsonResponse');
const {pathToStorage} = require('../constants');


class RequestFileByName extends BaseJsonResponse {
  async handleMainCode() {
    const pathToFile = path.resolve(pathToStorage, this.params.filename)
    const fileContent = await fs.readFile(pathToFile)

    this.responseData.message = 'Success'
    this.responseData.filename = this.params.filename
    this.responseData.content = fileContent.toString()
    this.responseData.extension = path.extname(pathToFile).slice(1)
    this.responseData.uploadedDate = (await fs.stat(pathToFile)).birthtime
  }
}

const getFileByName = (req, res) => {
  return new RequestFileByName(req, res).handle()
}

module.exports = getFileByName
