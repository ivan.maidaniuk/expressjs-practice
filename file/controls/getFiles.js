const fs = require('fs').promises

const {pathToStorage} = require('../constants');
const BaseJsonResponse = require('./BaseJsonResponse');

class RequestingFileSystem extends BaseJsonResponse {
  async handleMainCode() {
    this.responseData.files = await fs.readdir(pathToStorage)
    this.responseData.message = 'Success'
  }
}

const getFiles = (req, res) => {
  return new RequestingFileSystem(req, res).handle()
}

module.exports = getFiles
