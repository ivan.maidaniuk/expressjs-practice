const fs = require('fs').promises;
const {pathToStorage} = require('../constants');
const {exists} = require('../../utils/helperFunctions.js')

async function createStorageFolderIfNotExists(req, res, next) {
  if (!await exists(pathToStorage)) {
    console.log('!exists')
    await fs.mkdir(pathToStorage)
  }
  next()
}

module.exports = createStorageFolderIfNotExists
