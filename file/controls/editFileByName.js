const fs = require('fs').promises;
const path = require('path');

const BaseJsonResponse = require('./BaseJsonResponse');
const {StatusError} = require('../../utils/StatusError');
const {exists} = require('../../utils/helperFunctions');
const {pathToStorage} = require('../constants');

class FileAmending extends BaseJsonResponse {
  async handleMainCode() {
    const pathToFile = path.resolve(pathToStorage, this.params.filename)
    if (await exists(pathToFile)) {
      await fs.writeFile(pathToFile, this.props.content)
      this.responseData.message = 'File was edited successfully'
    } else {
      throw new StatusError(400, `There is no such file with name: '${this.params.filename}'`)
    }
  }
}

const editFileByName = (req, res) => {
  return new FileAmending(req, res).handle()
}

module.exports = editFileByName
