const fs = require('fs').promises
const path = require('path')

const BaseJsonResponse = require('./BaseJsonResponse');
const {StatusError} = require('../../utils/StatusError');
const {exists} = require('../../utils/helperFunctions');
const {pathToStorage, pathToStoragePermissionsFile} = require('../constants');


class FileDeletion extends BaseJsonResponse {
  async handleMainCode() {
    const pathToFile = path.resolve(pathToStorage, this.params.filename)
    if (await exists(pathToFile)) {
      await fs.unlink(pathToFile)
      await this.deletePassword()
      this.responseData.message = `Success. File \'${this.params.filename}\' has been deleted.`
    } else {
      throw new StatusError(400, `File \'${this.params.filename}\' doesn't exist.`)
    }
  }

  async deletePassword() {
    const permissions = JSON.parse(await fs.readFile(pathToStoragePermissionsFile, 'utf8'))
    delete permissions[this.params.filename]
    await fs.writeFile(pathToStoragePermissionsFile, JSON.stringify(permissions))
  }
}

const deleteFile = (req, res) => {
  return new FileDeletion(req, res).handle()
}

module.exports = deleteFile
