const express = require('express')
const router = express.Router()

const filePermissionMiddleware = require('./filePermissionMiddleware');
const validateRequestDataMiddleware = require('./validateRequestDataMiddleware');
const {
  createFile,
  getFiles,
  getFileByName,
  deleteFile,
  editFileByName,
  createStorageFolderIfNotExists
} = require('./controls')

router.use(createStorageFolderIfNotExists)
router.use('*/:filename', filePermissionMiddleware)


router.post('/', validateRequestDataMiddleware('body', ['filename', 'content']))
router.post('/', createFile)

router.get('/', getFiles)
router.get('/:filename', getFileByName)

router.get('/edit/:filename', getFileByName)

router.put('/edit/:filename', validateRequestDataMiddleware('body', ['content']))
router.put('/edit/:filename', editFileByName)

router.delete('/delete/:filename', deleteFile)


module.exports = {
  fileRouter: router
}
