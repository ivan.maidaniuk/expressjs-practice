const path = require('path');

const storageFolderName = 'storage'
const pathToStorage = path.resolve(storageFolderName)
const storagePermissionsFileName = 'storage-passwords.json'
const pathToStoragePermissionsFile = path.resolve(storagePermissionsFileName)

module.exports = {
  storageFolderName,
  pathToStorage,
  storagePermissionsFileName,
  pathToStoragePermissionsFile
}
