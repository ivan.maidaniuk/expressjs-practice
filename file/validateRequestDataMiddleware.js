const {log} = require('../utils/logger');
const {getEmptyRequiredFieldName} = require('../utils/helperFunctions');
const {StatusError} = require('../utils/StatusError');

function validateRequestDataMiddleware(reqPropertyToCheck, requiredFields) {
  return async function (req, res, next) {
    try {
      const emptyRequiredField = getEmptyRequiredFieldName(req[reqPropertyToCheck], requiredFields)
      if (emptyRequiredField) {
        throw new StatusError(400, `Please specify \'${emptyRequiredField}\' parameter`)
      }
      next()
    } catch (error) {
      log(error)
      if (error instanceof StatusError) {
        res.status(error.statusCode).json({message: error.message})
      }
      res.status(500).json({message: 'Server Error'})
    }
  }
}


module.exports = validateRequestDataMiddleware
