const validateRequestDataMiddleware = require('./validateRequestDataMiddleware');
const fs = require('fs').promises;

const {log} = require('../utils/logger');
const {exists} = require('../utils/helperFunctions');
const {pathToStoragePermissionsFile} = require('./constants');
const {StatusError} = require('../utils/StatusError');

async function filePermissionMiddleware(req, res, next) {
  try {
    if (!await hasPermissionToFile(req, res)) {
      await validateRequestDataMiddleware('query', ['password'])(req, res, next)
      throw new StatusError(403, 'Permission denied. Wrong password')
    }
    next()
  } catch (error) {
    log(error)
    if (error instanceof StatusError) {
      res.status(error.statusCode).json({message: error.message})
    }
    res.status(500).json({message: 'Server Error'})
  }
}

async function hasPermissionToFile(req, res) {
  if (await exists(pathToStoragePermissionsFile)) {
    const permissions = JSON.parse(await fs.readFile(pathToStoragePermissionsFile, 'utf8'))
    if (!permissions[req.params.filename]) {
      return true
    }
    return permissions[req.params.filename] === req.query.password;
  } else {
    return true
  }
}

module.exports = filePermissionMiddleware
